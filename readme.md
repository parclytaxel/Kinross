**Kinross: Python tools for mathematical vector graphics and Inkscape**  
Parcly Taxel / Jeremy Tan, 2019  
[Derpibooru](https://derpibooru.org/profiles/Parcly+Taxel) | [Weasyl](https://weasyl.com/~parclytaxel) | [Deviant Art](https://parcly-taxel.deviantart.com) | [Fur Affinity](https://furaffinity.net/user/parclytaxel) | [Inkbunny](https://inkbunny.net/Parclytaxel)

This repository was born from an Inkscape extension I wrote, Flevobézier, that fitted Bézier segments to a given list of points. This was the topic of my Advanced Research Project I needed to graduate from NUS High School. It did reasonably well, but as I improved my skills at making My Little Pony vector graphics (my hobby, and the reason the extension was wrote) I soon got into optimising the raw SVG code, whereupon I discovered that Inkscape would render paths affected by live path effects even if the _d_ attribute was deleted. Kinross is comprised of

* the resulting SVG optimiser **Rarify**
* the **Kinback** library that supports it
* a few other standalone scripts

The name comes from the Scottish town of Kinross; it requires [NumPy](http://numpy.org) as a dependency.

Rarify is better than the traditional [Scour](https://github.com/scour-project/scour) in two respects: it saves more bytes and preserves Inkscape editability. Outside Rarify, some sophisticated algorithms are implemented from scratch within, including Clenshaw–Curtis quadrature, line search (for the geometric median), a short method of determining self-intersections in a cubic Bézier curve through Viète's formulas and a rational univariate representation-based solver (with Newton tweaking) for curve intersection. The last goes against claims made by e.g. [Hunt Chang](https://sites.google.com/site/curvesintersection) and [Richard Kinch](http://truetex.com/bezint.htm) that no good algorithms for curve intersection exist (I considered Bézier clipping and implicitisation but rejected them as overly complicated).

I use Rarify to compress SVG files for my [Selwyn](https://gitlab.com/parclytaxel/Selwyn) repository. For very small files like cutie marks, however, I often proceed by hand for the compression, sometimes using [SVGOMG](https://jakearchibald.github.io/svgomg) as a first step.
