#!/usr/bin/env python3.7
# Stress tests for the geometric median.
import numpy as np
from kinback.pointset import geommedian

p1 = np.array([[0.95964001, 0.57039492], [0.49983666, 0.38651292], [0.24228648, 0.18275922], [0.92150915, 0.73461902]])
p2 = np.array([[0.95964001, 0.57039492], [0.5, 0.2], [0.24228648, 0.18275922], [0.92150915, 0.73461902]])
p3 = np.array([[0, 0], [1, 0], [2, 1.]])
p4 = np.array([[0, 0], [1, 0], [2, 1.75]])
print(geommedian(p1)) # [ 0.49983666  0.38651292]; one input point = median
print(geommedian(p2)) # [ 0.66761497  0.4125936 ]
print(geommedian(p3)) # [ 1.  0.]; one input point = median
print(geommedian(p4)) # [ 0.9982729   0.00297097]; median is very close to [1, 0]

p5 = np.random.normal(size=(20, 2))
print(geommedian(p5))

p6 = np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1], [10, 2, 4], [3, 5, 7]])
print(geommedian(p6)) # [ 0.52317899  0.36633564  0.48765276]

p7 = np.array([[10, 0], [0.2, 0.5], [-0.2, 1], [0.2, -0.5], [-0.2, -1]])
w7 = np.array([np.pi, 1, 1, 1, 1])
print(geommedian(p7)) # [  2.46388282e-01  -5.44585867e-17]
print(geommedian(p7, w7)) # [  9.25264363e-01   4.25311597e-17]
print(geommedian(p7, 2 * w7)) # same
print(geommedian(p7, w7 ** 1.208)) # [  9.36237759e+00  -6.29930176e-19]
print(geommedian(p7, w7[::-1])) # [ 0.08142232 -0.58200894]

p8 = np.random.normal(size=(3, 20))
p8 = (p8 / np.linalg.norm(p8, axis=0)).T
w8 = np.random.rand(20) + 1
print(geommedian(p8))
print(geommedian(p8, w8)) # these two computations should give close results

# With identical and "bad" points
p9 = np.array([[1, 2], [1, 2], [0, 10], [5, 4], [0, 10]])
print(geommedian(p9)) # [ 1.52606711  4.01103563]
w9 = np.array([0.5, 0.5, 0.5, 1, 0.5])
print(geommedian(p9, w9)) # [ 2.6058771   4.23755918]; equal-weight case, as evidenced by…
p10 = np.array([[1, 2], [5, 4], [0, 10]])
print(geommedian(p10)) # …this result, same as above
w10 = np.array([-0.3, 0.5, 0.5, 1, 0.5])
print(geommedian(p9, w10)) # [ 3.48555729  4.78178441]
