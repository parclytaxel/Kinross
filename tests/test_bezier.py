import numpy as np
from pytest import approx, mark
from kinross.matrices import mt
from kinross.paths import bezier

def test_bezier_data():
    B = bezier(1-2j, 5-1j, 4j, -11)
    assert str(B) == "bezier(1-2j,5-1j,4j,-11+0j)"
    P = np.array([1.933-1.593j, 2.251-0.371j, -2.801+1.521j, -7.883+0.943j])
    assert B([0.1, 0.3, 0.7, 0.9]) == approx(P)
    assert B[1] == 5-1j
    C = B[-0.5:1.5]
    C_ = np.array([-12.125+1.125j,15.375-11.375j,0.875+18.125j,-31.625-14.375j])
    assert C.v == approx(C_)
    assert C(0) == approx(B(-0.5))
    assert C(1) == approx(B(1.5))
    assert C[0.25:0.75].v == approx(B.v)
    assert (-B).v == approx(B[1:0].v)
    M = mt.parse("rotate(90)scale(-2)")
    B_ = M @ B
    assert B_(0.78) == approx(M @ B(0.78))

def test_bezier_ql():
    Q = bezier(6, 2+9j, -4-50j)
    assert Q(0.38) == approx(2.6712-2.9792j)
    L = bezier(1+1j, 2-1j)
    assert L(0.38) == approx(1.38+0.24j)

@mark.parametrize("curve, res",
    [((0, 15-5j, 17+5j, 12+3j), []),
     ((5j, 4-5j, 8+11j, 12+8j), [0.5777777777777777]),
     ((10j, 14+12j, 7+13j, 14+9j), [0.3173205132080006, 0.7415030162037641]),
     ((15j, 18+22j, -8+19j, 14+15j), []),
     ((3, 2-1j, 17+8j), []),
     ((299, 792+458j), [])])
def test_bezier_infl(curve, res):
    assert bezier(*curve).infl == approx(res)

def test_bezier_cx():
    L = bezier(15j, 18+22j, -8+19j, 14+15j)
    a, b = L(L.cx)
    assert a == approx(b)

def test_bezier_len():
    B = bezier(-21+6j, 36+6j, -36-27j, -5+6j)
    l1 = B.length(0.75, 0.25)
    l2 = B.length(0.8)
    assert B[0.25:0.75].length() == approx(l1)
    assert B[:0.8].length() == approx(l2)
    C = bezier(7+7j, 8-9j, -4-4j, -6+1j)
    assert C.t_length(C.length(-0.71)) == approx(-0.71)

def test_bezier_project():
    B = bezier(307+397j, 226-67j, 601+463j, 62+110j)
    assert B.project(331+219j) == approx(0.7191573485788018)
    assert B.project(53+73j) == 1

def test_bezier_curveinvert():
    C = bezier(6j, -2-4j, 18+4j, 7+13j)
    expC = np.array([[0.05511873, -0.02887906, 0.17327434],
                    [0.02374479, -0.07402787, 0.97982651]])
    assert C.t_inv == approx(expC)
    Q = bezier(1+3j, -2, 6-2j)
    expQ = np.array([[1/60, -11/60, 8/15], [0, 0, 1]])
    L = bezier(1+1j, -4-8j)
    expL = np.array([[-0.04716981, -0.08490566, 0.13207547], [0, 0, 1]])
    assert L.t_inv == approx(expL)

def test_bezier_implicit():
    # Indices to write implicit coefficients correctly into a square array
    ix = [0, 1, 0, 2, 1, 0, 3, 2, 1, 0]
    iy = [0, 0, 1, 0, 1, 2, 0, 1, 2, 3]
    
    B = bezier(1+10j, 30+24j, 31-17j, 15+11j)
    pB = np.zeros((4, 4))
    pB[(ix, iy)] = B.implicit
    test_xy = B(np.linspace(0, 1, 11))
    Z = np.polynomial.polynomial.polyval2d(test_xy.real, test_xy.imag, pB)
    assert max(abs(Z)) < 1e-12
    
    Q = bezier(26+20j, 36+12j, 20+72j)
    pQ = np.zeros((3, 3))
    pQ[(ix[:6], iy[:6])] = Q.implicit
    test_xy = Q(np.linspace(0, 1, 11))
    Z = np.polynomial.polynomial.polyval2d(test_xy.real, test_xy.imag, pQ)
    assert max(abs(Z)) < 1e-12
    
    L = bezier(874-28j, -3+538j)
    pL = np.zeros((2, 2))
    pL[(ix[:3], iy[:3])] = L.implicit
    test_xy = L(np.linspace(0, 1, 11))
    Z = np.polynomial.polynomial.polyval2d(test_xy.real, test_xy.imag, pL)
    assert max(abs(Z)) < 1e-12

@mark.parametrize("c1, c2, nx",
    [((1.2+0.8j, 7.9+2.6j), (1.2+3j, 4.8+0.1j), 1),
     ((1.2+3j, 4.8+0.1j), (2.1+1j, 4.6+2.6j, 2.3+0.7j), 2),
     ((2.7+2.6j, 3.6-1.2j, 1.3+2.6j, 4+1.5j), (1.2+0.8j, 7.9+2.6j), 3),
     ((2.1+1j, 4.6+2.6j, 2.3+0.7j), (2+3j, 4-5j, 3+6j), 4),
     ((2.7+2.6j, 3.6-1.2j, 1.3+2.6j, 4+1.5j), (2.1+1j, 4.6+2.6j, 2.3+0.7j), 6),
     ((-6-1j, 3+2j, 12+2j, 20-1j), (30+3j, 21, 12, 4+3j), 0),
     ((4+3j, 23+17j, 40+23j, 53+23j), (36+16j, 25+10j, 14+8j, 5+8j), 1),
     ((-6-1j, 3+2j, 12+2j, 20-1j), (-14-3j, 4+3j, 22+3j, 38-3j), 2),
     ((11+1j, 9+45j, 79+7j, 43+28j), (6+23j, 5+51j, 58-2j, 54+33j), 3),
     ((72+5j, 61+23j, 32+28j, 8+21j), (6+22j, 44+27j, 52+23j, 72+8j), 4),
     ((2+17j, 51-13j, -14+18j, 28+6j), (1+10j, 30+24j, 31-17j, 15+11j), 5),
     ((19+8j, 70+20j, 1+28j, 45+8j), (19+26j, 64+8j, 3+4j, 48+27j), 6),
     ((31+21j, 15j, 61+20j, 21+15j), (25+12j, 19+41j, 42-7j, 26+22j), 9),
     ((10+100j, 90+30j, 40+140j, 220+220j), (5+150j, 180+20j, 2+154j, 210+190j), 3),
     ((10+100j, 90+30j, 40+140j, 220+220j), (5+150j, 180+20j, 2+153.87109j, 210+189.87109j), 1)])
def test_bezier_x(c1, c2, nx):
    P, Q = bezier(*c1), bezier(*c2)
    inters = P.x_bez(Q)
    assert len(inters) == nx
    assert P(inters[:,0]) == approx(Q(inters[:,1]))
