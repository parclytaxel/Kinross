import numpy as np
from pytest import approx, raises
from kinross.matrices import mt

# Thanks to BCLegon (https://gitlab.com/BCLegon/pytest-ci-project-layout)
# for providing a template for using pytest with GitLab's CI.

def test_mt_data():
    A = np.array([[-0.2, -3.4, 8.7], [7, 0, -3.2], [0, 0, 1]])
    M = mt(-0.2, 7, -3.4, 0, 8.7, -3.2)
    assert M.m == approx(A)
    assert str(M) == "mt(-0.2,7.0,-3.4,0.0,8.7,-3.2)"
    # Deliberately mung the bottom row to test unary plus
    M.m[2] = [7, 7, 7]
    assert (+M).m == approx(A)
    assert M.lin == approx(A[:2,:2])
    assert M.aff == approx(A[:2,2])

def test_mt_primitives():
    P = mt.translate(-3) @ mt.rotate(-36, 4, 7) @ mt.scale(2, 7)
    Q = mt.matrix(1.61803398875, -1.17557050458, 4.11449676605,
                  5.66311896062, -6.35056474355, 3.68802204855)
    assert P.m == approx(Q.m)
    R = ~Q
    assert (Q @ R).m == approx(np.eye(3))
    assert (R @ Q).m == approx(mt.identity().m)
    assert P @ (-1-1j) == approx(-12.083095498344308-0.7995264074944259j)
    S = [-4.73253075+2.51245154j, -2.23606798+9.35114101j]
    assert P @ [1, 1j] == approx(S)
    assert P @ np.array([1, 1j]) == approx(S)

def test_mt_parse():
    P = mt.translate(-3) @ mt.rotate(-36, 4, 7) @ mt.scale(2, 7)
    Q = mt.parse("translate(-3)rotate(-36 4,7)scale(2. ,7.00)")
    assert P.m == approx(Q.m)
    T = mt.parse("scale(-.77)rotate(7,7,-77)skewX(7)")
    U = mt.parse("matrix(-0.76426054,-0.09383939,0,-0.77578257,7.1854571,1.0988144)")
    assert T.m == approx(U.m)

def test_mt_write():
    P = mt.parse("skewY(-8)skewY(8)rotate(72)rotate(108)")
    assert P.write_primitive() == "scale(-1)","must simplify rotate(180) to scale(-1) as Inkscape does"
    Q = mt(0.9999984769132877, 0.0017453283658983088, -0.0017453283658983088,
           0.9999984769132877, 0.17279664674420642, -0.010321184610871432)
    assert Q.write_primitive() == "rotate(.1 6 99)"
    assert mt.crush("translate(-3)rotate(-36 4,7)scale(2. ,7.00)") == "rotate(-36 2.5 11.616525)scale(2 7)"
    assert mt.crush("matrix(1,0,0,-1,0,0)") == "scale(1-1)"
    assert mt.crush("matrix(-1,0,0,1,0,0)") == "scale(-1 1)"

def test_mt_fit():
    p1 = [18, 10+4j, 13+10j]
    p2 = [-8-2j, 4-4j, 5+5j]
    P = mt.fit(p1, p2)
    Q = mt(-3.4 / 3, 0.8, 2.2 / 3, 1.1, 12.4, -16.4)
    assert P.m == approx(Q.m)
    R = mt.fit(p1, p2, proc=True)
    S = mt(-1.09629368943406, 0.7682057969756916, 0.7682057969756916,
            1.09629368943406, 11.73105336971226, -15.948183109360066)
    assert R.m == approx(S.m)
    assert R.write() == "rotate(-35.020079-19.409617-26.565806)scale(-1.338656 1.338656)"
    p3 = [0, 4-1j, -11-2j, 3j]
    with raises(ValueError):
        mt.fit(p1, p3)
