import numpy as np
from pytest import mark, raises
from kinross.pointset import chull

def test_chull():
    tri = [0.3-2.7j, 4.1+3.8j, -2.2-1.5j]
    assert chull(tri) == [2, 0, 1]
    assert chull(tri[::-1]) == [0, 2, 1]
    
    p7 = [1+1j, -2, -3-4j, -1-3j, -3-2j, -1-4j, -2-2j]
    assert chull(p7) == [2, 5, 0, 1, 4]
    
    theta = np.linspace(0, 2 * np.pi, 501, False)
    ring = 9.9 * np.cos(theta) + 3.7j * np.sin(theta)
    assert chull(ring) == list(range(251, 501)) + list(range(251))
    
    with raises(ValueError):
        chull([0, 1])

@mark.parametrize("N, l", [(20, 8), (100, 13), (500, 21), (2000, 34),
                           (10000, 55), (20000, 89), (50000, 144), (200000, 233)])
def test_chull_phyllo(N, l):
    theta = np.arange(N) * np.pi * (3 - np.sqrt(5))
    r = 0.01 * np.sqrt(np.arange(N))
    phyllo = r * (np.cos(theta) + 1j * np.sin(theta))
    assert len(chull(phyllo)) == l
