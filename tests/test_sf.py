from kinross.regexes import sf, ssf

def test_sf():
    assert sf(0) == "0"
    assert sf(1e-9) == "0"
    assert sf(0.000000000999) == "0"
    assert sf(-1e-8) == "-1e-8"
    assert sf(0.00004567) == "4567e-8"
    assert sf(-0.000999) == "-999e-6"
    assert sf(-0.00100001) == "-.00100001"
    assert sf(3.14159) == "3.14159"
    assert sf(-1726.00001) == "-1726"
    assert sf(1726000.1) == "1726000.1"
    assert sf(-172600000) == "-1726e5"

def test_ssf():
    assert ssf(336, -4, 8.9, 0.718, -7, -7e9, 0.00004719, 0.81119,
           8110000, 0.6174) == "336-4 8.9.718-7-7e9 4719e-8.81119 811e4.6174"
