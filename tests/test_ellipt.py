import numpy as np
from pytest import approx, mark, raises
from kinross.matrices import mt
from kinross.paths import ellipt, bezier
from kinross.bernpol import x_conic

def test_ellipt_data():
    E = ellipt(mt(4, -2, 3, 2, -1, 0))
    assert str(E) == "ellipt(mt(4.0,-2.0,3.0,2.0,-1.0,0.0), [0, 6.283185307179586])"
    P = np.array([3-2j, 2+2j, 0.61710157+2.52014702j, -5.99942373+0.44246348j, 0.47271222-2.79360449j])
    assert E([0, 0.25, 0.3, 0.6, 0.9]) == approx(P)
    assert E(1) == approx(P[0])
    F = E[:0.5]
    assert F.ends == approx([0, np.pi])
    assert F(0.3) == approx(E(0.15))
    assert (-E)(np.array(0.9)) == approx(E(0.1))
    M = mt.parse("rotate(90)scale(-2)")
    E_ = M @ E
    assert E_(0.46) == approx(M @ E(0.46))

def test_ellipt_orth():
    E = ellipt(mt(4, -2, 3, 2, -1, 0))
    a = np.array([[-4.98966471, -0.32131925], [0.57911196, -2.7685067]])
    assert E.orth.lin == approx(a)
    F = E[0.9:0.1]
    Fo = F.orth
    assert Fo([0, 1]) == approx(E([0.9, 0.1]))
    assert Fo([-0.4, 0.3, 0.8, 1.1]) == approx(F([-0.4, 0.3, 0.8, 1.1]))
    G = ellipt(mt(3, 1, -2, -2, 4, 5), [-0.3, -1])
    assert G.orth([0.4, 0.6]) == approx(G([0.4, 0.6]))

def test_ellipt_fit():
    p = [1+1j, 6+1j, 7+4j, 3+6j, 4j]
    C = ellipt.fit_circle(p)
    assert C.aff == approx([3.5154860912, 3.05434470892])
    assert C.lin[0,0] == approx(3.35179839804)
    E = ellipt.fit(p)
    assert E.aff == approx([3.5, 3.125])
    assert E.lin == approx(np.diag([3.6708309686, 2.90204669156]))
    
    with raises(ValueError):
        ellipt.fit_circle(p[:2])
    with raises(ValueError):
        ellipt.fit(p[:4])

def test_ellipt_len():
    E = ellipt(mt(4, -2, 3, 2, -1, 0), [5, 2])
    assert E.length(0.7) == approx(7.534028119112152)
    assert E.length(0.3, 0.9) == approx(-6.618847407071457)
    assert E.t_length(E.length(0.6)) == approx(0.6)

def test_ellipt_project():
    E = ellipt(mt(10, 1, 0, 2, 2, 1), [0, 4])
    p = [0, -1+5j, 10, 10+1j]
    Ep = [E.project(point) for point in p]
    t = [0.453390345149, 0.459459133993, 0, 0.143173184954]
    assert Ep == approx(t)
    F = ellipt(mt.scale(2, 1))
    assert F.project(0.3j) == approx(0.25)
    assert F.project(2+1j) == approx(0.0934589301247)
    assert F(F.project(0.3)).real == approx(0.4)
    assert F(F.project(0)).real == approx(0)

@mark.parametrize("m, ends",
    [((1, 6, -1, 5, -4, -4), [0.5, 3]),
     ((3, 0, 1, -4, 8, -2), [1.5, -1.4])])
def test_ellipt_curveinvert(m, ends):
    E = ellipt(mt(*m), ends)
    assert E.inv(E(0.6)) == approx(0.6)
    assert E.inv(E(1.2)) == approx(1.2)
    assert E(E.inv(E(-0.2))) == approx(E(-0.2))
    assert E.inv(E([0.1, 0.7, 0.9])) == approx([0.1, 0.7, 0.9])

def test_ellipt_qmat():
    E = ellipt(mt(4, -2, 3, 2, -1, 0))
    Q = E.qmat
    test_xy = E(np.linspace(0, 1, 10, endpoint=False))
    J = np.array([test_xy.real, test_xy.imag, np.ones(10)])
    Z = np.diag(J.T @ Q @ J)
    assert max(abs(Z)) < 1e-12

def test_ellipt_x_bezier():
    E = ellipt(mt.rotate(18) @ mt.transcale(33.183277, 11.446495, 57.155113, 26.495396))
    B = bezier(32.617752+9.3821024j, -20.27971+70.67357j, 83.746729+40.795477j, 55.06798+62.307907j)
    i1 = B.x_ell(E)
    assert len(i1) == 4
    assert B(i1[:,0]) == approx(E(i1[:,1]))
    E = E[0.187:0.856]
    i2 = E.x_bez(B)
    assert len(i2) == 3 # truncating the ellipse suppresses one solution
    assert E(i2[:,0]) == approx(B(i2[:,1]))

@mark.parametrize("m1, ends1, m2, ends2, nx",
    [((10, 1, 0, 2, 2, 1), None, (3, -1, 2, 4, -3, -1), None, 4),
     ((10, 1, 0, 2, 7, 1), None, (3, -1, 2, 4, -3, -1), None, 2),
     ((10, 1, 0, 2, 12, 1), None, (3, -1, 2, 4, -3, -1), None, 0),
     ((5, 1, 0, 4, 2, 1), None, (1, 0, 0, 1, 0, 0), None, 0),
     ((1, 0, 0, 1, 0, 0), None, (2, 0, 0, 2, 0, 0), None, 0),
     ((1, 0, 0, 2, 0, 0), None, (2, 0, 0, 1, 0, 0), None, 4),
     ((4, -2, -1, 5, 2, 2), [-1.4, 4.8], (3, 0, -6, -1, 1, -4), [-1.9, 4.3], 0),
     ((4, -2, -1, 5, 2.5, 2), [4.8, -1.4], (3, 0, -6, -1, 1, -4), [-1.9, 4.3], 1),
     ((4, -2, -1, 5, 2.5, 2), [4.8, -1.4], (3, 0, -6, -1, 1, 2), [-1.9, 4.3], 3)])
def test_ellipt_x_ellipt(m1, ends1, m2, ends2, nx):
    E = ellipt(mt(*m1), ends1)
    F = ellipt(mt(*m2), ends2)
    inters = E.x_ell(F)
    assert len(inters) == nx
    assert E(inters[:,0]) == approx(F(inters[:,1]))
