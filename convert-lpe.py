#!/usr/bin/env python3
import glob
import xml.etree.ElementTree as ET
from kinross.constants import SVG_NAMESPACES, INK_
for k, v in SVG_NAMESPACES.items(): ET.register_namespace(k, v)

subs = {"interpolator_type": "CubicBezierFit",
        "linejoin_type": "extrp_arc",
        "start_linecap_type": "butt",
        "end_linecap_type": "butt"}

flist = [s for s in glob.glob("*.svg") if not s.endswith("-2.svg")]
for f in flist:
    tree = ET.parse(f)
    for pe in tree.findall(".//" + INK_ + "path-effect"):
        if pe.get("effect", "") == "powerstroke":
            for (k, v) in subs.items():
                if pe.get(k) is None:
                    pe.set(k, v)
    with open(f"{f[:-4]}-2.svg", 'w') as outf:
        tree.write(outf, "unicode")
