# Colour conversions and solvers
from .constants import X11, X11S, DECS
import numpy as np

# In no case does the opacity ever appear attached to a colour in SVG files. The two are always separate;
# only the fill and stroke dialogue in Inkscape shows them combined as an eight-hex value.
# To keep things simple, no NumPy functions are called here.
def col2rgb(col):
    """Converts an SVG colour into a 3-list of values between 0 and 1."""
    if col in X11: return [i / 255 for i in X11[col]]
    elif "(" in col: # Bracketed colours
        ket = col[4:-1].replace(" ", "").split(",")
        if col[0].lower() == 'r': return [float(c[:-1]) / 100 if ket[0][-1] == "%" else float(c) / 255 for c in ket] # rgb()
        else: # hsl(); algorithm used here is the canonical one given at https://www.w3.org/TR/css3-color/#hsl-color
            h, s, l = float(ket[0]) / 360, float(ket[1][:-1]) / 100, float(ket[2][:-1]) / 100
            m2 = l + s - l * s if l > 0.5 else l * (s + 1)
            m1 = 2 * l - m2
            def ZH(y):
                z = y % 1
                if z * 6 < 1: return m1 + 6 * z * (m2 - m1)
                if z * 2 < 1: return m2
                if z * 3 < 2: return m1 + (4 - 6 * z) * (m2 - m1)
                return m1
            return [ZH(h + 1 / 3), ZH(h), ZH(h - 1 / 3)]
    else: # 3/6 hexes
        if len(col) == 4: return [int(2 * col[i], 16) / 255 for i in range(1, 4)]
        else: return [int(col[i:i + 2], 16) / 255 for i in range(1, 7, 2)]
def rgb2col(rgb):
    """Returns the shortest representation of the RGB list (alias or 3/6 hexes)."""
    quant = tuple(round(255 * comp) for comp in rgb)
    if quant in X11S: return X11S[quant]
    return "#" + "".join(["{:02x}".format(n) if max(n % 17 for n in quant) else "{:x}".format(n >> 4) for n in quant])
def shortcolour(c):
    """Given an SVG colour (which may actually be nothing or a URI), returns its shortest representation."""
    return c if c == "none" or c[0] == 'u' else rgb2col(col2rgb(c))
def shortopacity(d):
    """The same as shortcolour, but working on opacities."""
    return DECS[round(float(d) * 255)]

# The following functions accept and return NumPy vectors of 3 elements (nominally) in [0, 1]; alphas are provided separately.
# The below functions convert between sRGB, CIEXYZ (D65) and CIELAB.
def rgb_lin(k): return k / 12.92 if k <= 0.040449936 else ((k + 0.055) / 1.055) ** 2.4
def rgb_inv(k): return 12.92 * k if k <= 0.0031308 else 1.055 * k ** (1 / 2.4) - 0.055
x2rmat = np.array([[3.2406, -1.5372, -0.4986], [-0.9689, 1.8758, 0.0415], [0.0557, -0.2040, 1.0570]])
r2xmat = np.linalg.inv(x2rmat)
def xyz2rgb(c): return np.array([rgb_inv(k) for k in x2rmat @ c])
def rgb2xyz(c): return r2xmat @ [rgb_lin(k) for k in c]
def lumroot(k): return k ** (1 / 3) if k > 216 / 24389 else k * 841 / 108 + 4 / 29
def lumcube(k): return k ** 3 if k > 6 / 29 else 108 / 841 * (k - 4 / 29)
def xyz2lab(c):
    lyn = lumroot(c[1])
    return np.array([116 * lyn - 16, 500 * (lumroot(c[0] / 0.95047) - lyn), 200 * (lyn - lumroot(c[2] / 1.08883))])
def lab2xyz(c):
    l0 = (c[0] + 16) / 116
    return np.array([0.95047 * lumcube(l0 + c[1] / 500), lumcube(l0), 1.08883 * lumcube(l0 - c[2] / 200)])
def rgb2lab(c): return xyz2lab(rgb2xyz(c))
def lab2rgb(c): return xyz2rgb(lab2xyz(c))

# RGB alpha compositing functions. Let the (back)ground be B, the "source" (tint) S and the result (comp) R, then
# R_alpha = S_alpha + B_alpha * (1 - S_alpha)
# R_red = (S_red * S_alpha + B_red * B_alpha * (1 - S_alpha)) / R_alpha
# R_red = 0 if R_alpha = 0
# and same for the other two primaries
def alphacomp(back, back_alpha, tint, tint_alpha): # tint over back = comp
    comp_alpha = back_alpha * (1 - tint_alpha) + tint_alpha
    if abs(comp_alpha) < 1e-8: return np.zeros(3), 0
    return (tint * tint_alpha + back * back_alpha * (1 - tint_alpha)) / comp_alpha, alpha
# B_alpha = (R_alpha - S_alpha) / (1 - S_alpha)
# B_red = (R_red * R_alpha - S_red * S_alpha) / (R_alpha - S_alpha)
def alphaback(tint, tint_alpha, comp, comp_alpha):
    if abs(tint_alpha - 1) < 1e-8: return np.zeros(3), 1
    if abs(tint_alpha - comp_alpha) < 1e-8: return np.zeros(3), 0
    return (comp * comp_alpha - tint * tint_alpha) / (comp_alpha - tint_alpha), (comp_alpha - tint_alpha) / (1 - tint_alpha)
# This only works if B_alpha != 1
# S_alpha = (R_alpha - B_alpha) / (1 - B_alpha)
# S_red = (R_red * R_alpha - B_red * B_alpha * (1 - S_alpha)) / S_alpha
# S_red = 0 if S_alpha = 0
def alphatint_simple(back, back_alpha, comp, comp_alpha):
    if abs(back_alpha - 1) < 1e-8: raise ValueError("simple algorithm does not work for opaque background")
    tint_alpha = (comp_alpha - back_alpha) / (1 - back_alpha)
    if abs(tint_alpha) < 1e-8: return np.zeros(3), 0
    return (comp * comp_alpha - back * back_alpha * (1 - tint_alpha)) / tint_alpha
# If B_alpha = 1 the forward equations become
# R = S * S_alpha + B * (1 - S_alpha)
# which rearranges to the linear
# S + 1/S_alpha (B - R) = B
# with x = [S_red, S_green, S_blue, 1/S_alpha] and for each colour pair
#    [1, 0, 0,     B_red - R_red]    [  B_red]
# A: [0, 1, 0, B_green - R_green] b: [B_green]
#    [0, 0, 1,   B_blue - R_blue]    [ B_blue]
# backs and comps can be either lists of triples or N×3 arrays.
def alphatint(backs, comps):
    if len(backs) != len(comps): raise TypeError("colour sets differ in length")
    if len(backs) < 2: raise TypeError("computing the tint requires at least two pairs of colours")
    A = np.block([[np.eye(3), (backs[i] - comps[i]).reshape(3, 1)] for i in range(len(backs))])
    b = np.concatenate(backs)
    x = np.linalg.lstsq(A, b)[0]
    if abs(x[3]) < 1e-8: return np.zeros(3), 0
    return x[:3], 1 / x[3]

def lablerp(c1, c2, t = 0.5):
    """Returns the colour t of the way from c1 to c2 under CIELAB. Inputs are 6/8-hex RGB(A) strings; output is a 6-hex RGB string (alpha is ignored)."""
    l1, l2 = (rgb2lab([int(c[i:i + 2], 16) / 255 for i in (0, 2, 4)]) for c in (c1, c2))
    l = np.clip(lab2rgb(l1 * (1 - t) + l2 * t) * 255, 0, 255)
    return "".join(["{:02x}".format(round(n.item())) for n in l])
