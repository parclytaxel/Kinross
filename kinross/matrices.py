# Affine transformation matrices
import numpy as np
from numbers import Number
from collections.abc import Sequence
from .regexes import sf, ssf, mtprim_re, number_re

# mt represents a 3×3 transformation matrix, wrapping around a same-sized np.array m
# whose bottom row is [0, 0, 1]. Slices of an mt return np.array.
class mt:
    def __init__(self, *params):
        """If params is an np.array, initialise an mt instance with that array,
        resetting the three bottom entries to [0, 0, 1]. Otherwise, construct
        an np.array from the six given parameters in the order used by SVG matrix()."""
        if len(params) == 1:
            self.m = params[0].copy()
            self.m[2:] = [0, 0, 1.]
        if len(params) == 6:
            a, b, c, d, e, f = params
            self.m = np.array([[a, c, e], [b, d, f], [0, 0, 1.]])
    def __str__(self):
        """Return a string representation of this mt instance with values in SVG order."""
        return f"mt({','.join(str(x) for x in self.c.flatten('F'))})"
    def __repr__(self):
        return str(self)
    def __pos__(self):
        """Return a normalised (bottom row reset to [0, 0, 1]) new copy of
        this mt instance, +M being a convenient shorthand."""
        return mt(self.m)
    
    # Matrix slices, which return standard np.arrays. The transformation matrix
    # [a c e]
    # [b d f]
    # [0 0 1]
    # sends the origin to (e, f) (the affine or translational part)
    # and, relative to this new origin, (1, 0) to (a, b) and (0, 1) to (c, d).
    # The upper-left 2×2 submatrix is the linear part, which preserves the origin.
    @property
    def c(self):
        """Return the 2×3 coefficient array of this mt instance."""
        return self.m[:2]
    @property
    def lin(self):
        """Return the linear part of this mt instance."""
        return self.m[:2,:2]
    @property
    def ux(self):
        """Return the image of the unit x-vector under this mapping."""
        return self.m[:2,0]
    @property
    def uy(self):
        """Return the image of the unit y-vector under this mapping."""
        return self.m[:2,1]
    @property
    def aff(self):
        """Return the affine or translational part of this mt instance."""
        return self.m[:2,2]
    
    # Transformation primitives, as defined in the SVG standard.
    # Angular arguments are measured in degrees.
    def identity():
        return mt(1, 0, 0, 1, 0, 0)
    def matrix(a, b, c, d, e, f):
        return mt(a, b, c, d, e, f)
    def translate(dx, dy=0):
        return mt(1, 0, 0, 1, dx, dy)
    def scale(sx, sy=None):
        return mt(sx, 0, 0, sx if sy == None else sy, 0, 0)
    def transcale(sx, sy, dx, dy): # convenience function
        return mt(sx, 0, 0, sy, dx, dy)
    def rotate(th, cx=0, cy=0):
        c, s = np.cos(np.radians(th)), np.sin(np.radians(th))
        return mt(c, s, -s, c, (1 - c) * cx + s * cy, (1 - c) * cy - s * cx)
    def skewx(z):
        return mt(1, 0, np.tan(np.radians(z)), 1, 0, 0)
    def skewy(z):
        return mt(1, np.tan(np.radians(z)), 0, 1, 0, 0)
    def __matmul__(self, other):
        """If the argument to the right is a number or sequence of numbers,
        applies this mapping to said arguments. If an mt instance,
        performs transform composition. For other types of data, define
        __rmatmul__ in the other object."""
        if isinstance(other, (Number, Sequence, np.ndarray)):
            return [1, 1j] @ self.c @ [np.real(other), np.imag(other), np.ones_like(other)]
        if isinstance(other, mt):
            return mt(self.m @ other.m)
        return NotImplemented
    def __invert__(self):
        """Returns the inverse of this mt instance, called using ~M."""
        return mt(np.linalg.inv(self.m))
    
    primdict = {"matrix": matrix, "translate": translate, "scale": scale,
                "rotate": rotate, "skewX": skewx, "skewY": skewy}
    def parse(s):
        """Parse the input SVG transform attribute, returning an mt instance."""
        out = mt.identity()
        for prim in mtprim_re.finditer(s):
            cmd, payload = prim.groups()
            out @= mt.primdict[cmd](*(float(n) for n in number_re.findall(payload)))
        return out
    def write_primitive(self):
        """If this mapping can be written using one SVG transformation primitive,
        return a string representation using that primitive. Otherwise, return a
        "default" representation using matrix().
        
        The identity is also tested for, in which case the empty string is returned."""
        # Translation
        if np.allclose(self.lin, np.eye(2), rtol=1e-8):
            dx, dy = self.aff
            if abs(dy) > 1e-8:
                return f"translate({ssf(dx, dy)})"
            if abs(dx) > 1e-8:
                return f"translate({sf(dx)})"
            return ""
        # Scaling
        if np.allclose(self.m, np.diag(np.diagonal(self.m))):
            sx, sy = np.diag(self.lin)
            if not np.isclose(sy, sx):
                return f"scale({ssf(sx, sy)})"
            if abs(sx) > 1e-8:
                return f"scale({sf(sx)})"
            return ""
        # Rotation (both one-argument and three-argument)
        if np.isclose(np.linalg.det(self.lin), 1):
            cth, sth = self.ux
            cx, cy = np.linalg.solve([[1 - cth, sth], [-sth, 1 - cth]], self.aff)
            theta = np.degrees(np.arctan2(sth, cth))
            if np.hypot(cx, cy) > 1e-8:
                return f"rotate({ssf(theta, cx, cy)})"
            return f"rotate({sf(theta)})"
        # Default matrix()
        return f"matrix({ssf(*self.c.flatten('F'))})"
    def conformal_decomp(self):
        """If possible, decompose this mapping conformally as M = RS, where R is a rotation
        and S is a scaling with sign chosen to minimise the absolute angle in R.
        Returns one mt instance for each, and None if the decomposition is not possible.
        A mapping m is conformal iff m.ux · m.uy = 0."""
        if abs(self.ux @ self.uy) > 1e-8:
            return None
        sign = 1 if self.m[0,0] >= 0 else -1
        det = 1 if np.linalg.det(self.lin) >= 0 else -1
        sx = np.linalg.norm(self.ux) * sign
        sy = np.linalg.norm(self.uy) * sign * det
        sdiag = np.array([sx, sy, 1])
        return (mt(self.m / sdiag), mt.scale(sx, sy))
    def write(self):
        """If this mapping can be written as one SVG primitive, do so.
        If not, and it is conformal, return the shorter of the conformal representation
        and the matrix()-based representation."""
        primrep = self.write_primitive()
        if not primrep.startswith("matrix"):
            return primrep
        conformaltup = self.conformal_decomp()
        if conformaltup == None:
            return primrep
        conformalrep = "".join(p.write_primitive() for p in conformaltup)
        return conformalrep if len(conformalrep) <= len(primrep) else primrep
    def crush(s):
        """Return a shortened version of the SVG transformation attribute s."""
        return mt.parse(s).write()
    
    def fit(s1, s2, proc=False):
        """Return the least-squares matrix mapping s1 to s2, both sequences
        of complex numbers. If proc is True, force this matrix to be Procrustes
        (i.e. uniform scaling and no skewing)."""
        if len(s1) != len(s2):
            raise ValueError("sequence lengths must be equal")
        s1 = np.stack([np.real(s1), np.imag(s1), np.ones_like(s1, dtype=float)], axis=1)
        s2 = np.stack([np.real(s2), np.imag(s2)], axis=1)
        if proc:
            s1 = s1[:,:2]
            mu1, mu2 = s1.mean(axis=0), s2.mean(axis=0)
            sigma1 = np.hypot(*s1.std(axis=0))
            sigma2 = np.hypot(*s2.std(axis=0))
            n1, n2 = (s1 - mu1) / sigma1, (s2 - mu2) / sigma2
            u, sigma, v = np.linalg.svd(n1.T @ n2)
            return mt.translate(*mu2) @ mt.scale(sigma2) @ mt(*(u @ v).flatten(),
                   0, 0) @ mt.scale(1 / sigma1) @ mt.translate(*(-mu1))
        return mt(*np.linalg.lstsq(s1, s2, rcond=None)[0].flatten())
