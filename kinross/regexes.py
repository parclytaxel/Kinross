# Regexes and string manipulation
import re
from math import log10, floor
# Regex matching an SVG transformation primitive
mtprim_re = re.compile(r"(matrix|translate|scale|rotate|skewX|skewY)\s*\((.*?)\)")
# Regex matching a single number
number_re = re.compile(r"[-+]?(?:(?:[0-9]*\.[0-9]+)|(?:[0-9]+\.?))(?:[eE][-+]?[0-9]+)?")

pcomm_re = re.compile("([MZLHVCSQTAmzlhvcsqta])([^MZLHVCSQTAmzlhvcsqta]*)")
rotate1_re = re.compile(r"rotate\(.*?\)")
sw_re = re.compile(r"stroke-width:(" + number_re.pattern + ")")

def sf(x, dp=8):
    """Return the Shortest unambiguous string representation of Float x,
    limiting to 8 significant figures and the given number of decimal places (dp)."""
    if round(x, dp) == 0: return "0"
    a = abs(x)
    a = round(a, min(dp, 8 - max(floor(log10(a)) + 1, 0)))
    if a % 1000 == 0: # positive exponent is better, e.g. 137000 = 137e3
        i = str(int(a))
        cf = i.rstrip('0')
        res = f"{cf}e{len(i) - len(cf)}"
    elif a < 0.001: # negative exponent is better, e.g. .00137 = 137e-5
        v = f"{a:.8f}".rstrip('0')[2:]
        res = f"{v.lstrip('0')}e-{len(v)}"
    else: res = str(a).strip('0').rstrip('.')
    return '-' * int(x < 0) + res
    
def ssf(*l, dp=8):
    """Return the Shortest unambiguous string representation of the Sequence
    of Floats l, limiting to 8 sf and the given dp."""
    lshort = [sf(x, dp) for x in l]
    spaces = ["" if b[0] == "-" or b[0] == "." and ("." in a or "e" in a)
             else " " for (a, b) in zip(lshort, lshort[1:])]
    out = [None] * (2 * len(lshort) - 1)
    out[::2] = lshort
    out[1::2] = spaces
    return "".join(out)
