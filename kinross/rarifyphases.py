# Components (historically called phases) of the Rarify SVG optimiser
from .constants import SVG_, INK_, SOD_, XLN_, NONSTYLE_DEFAULTS, NONSTYLE_DEFAULTS_LPE, STYLE_DEFAULTS, STYLE_DEFAULTS_TEXT, STYLE_DEFAULTS_INKTEXT, STYLE_PROPERTIES
from .regexes import rotate1_re, sw_re
from .colours import shortcolour, shortopacity
from .matrices import mt
from .paths import ellipt

# Paint order simplification lookup table
porders = {"fill stroke": "normal",
           "stroke fill": "stroke",
           "markers fill": "markers"}

def mapminus(sd, dct, cond = {}):
    """Removes keys IN-PLACE from sd according to the default dictionary dct. cond gives conditions; all keys in cond must be in sd
    and the corresponding values must match. None can be used in dct and cond to match any value."""
    for c in cond:
        if c not in sd or cond[c] not in (None, sd[c]): return
    k = list(sd.keys())
    for i in k:
        if i in dct and dct[i] in (None, sd[i]): del sd[i]

def rarify_prunetree(tr, flags):
    """Tree-level simplifications for Rarify. tr is the ElementTree and flags comes from the command-line arguments."""
    bloat = tr.findall(SOD_ + "namedview")
    if flags.metadata:
        bloat.extend(tr.findall(SVG_ + "metadata"))
        bloat.extend(tr.findall(SVG_ + "title"))
    if flags.scripts:
        bloat.extend(tr.findall(SVG_ + "script"))
    for b in bloat: tr.getroot().remove(b)
    if flags.dimens: [tr.getroot().attrib.pop(s, 0) for s in ("width", "height")]
    # Embrittlement of zero-length groups
    N = 1
    while N:
        N = 0
        sel = tr.findall(".//" + SVG_ + "g/..")
        for par in sel:
            torm = []
            for nd in list(par):
                if nd in sel: continue
                if nd.tag == SVG_ + "g" and not list(nd): torm.append(nd)
            for degen in torm: par.remove(degen)
            N += len(torm)

def popstyle(node, quiet = False):
    """Pops style attributes out of node, returning a dictionary. If quiet is True, assume everything is already properly formatted in a style attribute."""
    if quiet: return dict(x.split(':') for x in node.attrib.pop("style", "").split(';'))
    styprops = {prop: node.attrib.pop(prop) for prop in STYLE_PROPERTIES if prop in node.attrib.keys()}
    styprops.update(dict(x.strip().split(':') for x in node.attrib.pop("style", "").split(';') if ':' in x))
    return styprops

def setstyle(node, styprops):
    """Sets the style attribute of a node based on the dictionary styprops. Does not perform presentation attribute splitting."""
    if styprops: node.set("style", ";".join(f"{k}:{v}" for k, v in styprops.items()))

def getreferences(tr):
    """Scrapes references out of the tree. Returns a dictionary whose keys are nodes and whose corresponding values are
    dictionaries showing the XML links they partake in."""
    nodes_in_links = {}
    for node in tr.iter():
        styprops, refs = dict(x.strip().split(':') for x in node.attrib.get("style", "").split(';') if ':' in x), {}
        for a in ("fill", "stroke", "clip-path", "mask", "filter"):
            if styprops.get(a, "a")[0] == 'u': refs[a] = styprops[a][5:-1]
        tmp = node.get(XLN_ + "href")
        if tmp: refs["use"] = tmp[1:]
        tmp = node.get(INK_ + "path-effect")
        if tmp: refs["path-effect"] = tmp[1:]
        if refs: nodes_in_links[node] = refs
    return nodes_in_links

def getids(tr):
    """Scrapes IDs out of the tree. Returns a dictionary mapping IDs to their corresponding nodes."""
    ids_to_nodes = {}
    for node in tr.iter():
        tmp = node.get("id")
        if tmp: ids_to_nodes[tmp] = node
    return ids_to_nodes

def rarify_node(tr, flags):
    """Node-level simplifications for Rarify, including links. The bulk of the code concerns styling properties;
    for simplicity these are left in the node's style attribute at the end and consolidation happens later."""
    for rule in NONSTYLE_DEFAULTS: # Handle non-style attributes
        for node in tr.iter(rule[0]): mapminus(node.attrib, rule[1], rule[2])
    if flags.lpecrush:
        for rule in NONSTYLE_DEFAULTS_LPE:
            for node in tr.iter(rule[0]): mapminus(node.attrib, rule[1], rule[2])
    
    clipchildren = [cc for cp in tr.iter(SVG_ + "clipPath") for cc in cp]
    for node in tr.iter():
        styprops = popstyle(node)
        if not styprops: continue
        if node in clipchildren: # For children of clipping paths, only clip-path and clip-rule are relevant
            node.attrib.update({prop: styprops[prop] for prop in ("clip-path", "clip-rule") if prop in styprops})
            if styprops.get("clip-rule", "nonzero") != "evenodd" or node.tag in ("circle", "ellipse", "rect"): styprops.pop("clip-rule", 0)
            continue
        # Colour, opacity
        for col in styprops.keys() & ("fill", "stroke", "stop-color", "color", "solid-color", "flood-color", "lighting-color", "text-decoration-color"): styprops[col] = shortcolour(styprops[col])
        for opa in styprops.keys() & ("fill-opacity", "stroke-opacity", "stop-opacity", "opacity", "solid-opacity", "flood-opacity"): styprops[opa] = shortopacity(styprops[opa])
        # Paint order
        if styprops.get("paint-order", "normal") != "normal":
            pm = styprops["paint-order"]
            if pm.count(" ") == 2: pm = pm[:pm.rfind(" ")] 
            m0 = styprops.get("marker-start", "none") == "none" and styprops.get("marker-mid", "none") == "none" and styprops.get("marker-end", "none") == "none"
            if m0: pm = "stroke" if "stroke" in pm and not pm.startswith("fill") else "normal"
            else:
                if pm in porders: pm = porders[pm]
            styprops["paint-order"] = pm
        # Properties rendered useless if other properties are set certain ways
        if styprops.get("stroke-dasharray", "none") == "none": styprops.pop("stroke-dashoffset", 0)
        if styprops.get("stroke", "none") == "none":
            for spr in ("stroke-opacity", "stroke-width", "stroke-linejoin", "stroke-linecap", "stroke-miterlimit", "stroke-dasharray", "stroke-dashoffset"): styprops.pop(spr, 0)
        if styprops.get("stroke-linejoin", "miter") != "miter": styprops.pop("stroke-miterlimit", 0)
        if styprops.get("fill") == "none": styprops.pop("fill-rule", 0)
        # Default property removal
        mapminus(styprops, STYLE_DEFAULTS)
        if node.tag.endswith("}text") or node.tag.endswith("}tspan"):
            mapminus(styprops, STYLE_DEFAULTS_TEXT)
            mapminus(styprops, STYLE_DEFAULTS_INKTEXT)
        else:
            for k in STYLE_DEFAULTS_TEXT: styprops.pop(k, 0)
        # <g>, <text>: remove inheritable style properties on children
        if node.tag.endswith("}g") or node.tag.endswith("}text"):
            for child in list(node):
                cstyle = popstyle(child)
                mapminus(cstyle, styprops)
                setstyle(child, cstyle)
        setstyle(node, styprops)

def rarify_references(tr):
    nodes_in_links, ids_to_nodes = getreferences(tr), getids(tr)
    links = {item for node in nodes_in_links for item in set(nodes_in_links[node].values())}
    # Remove unreferenced IDs
    id2 = list(ids_to_nodes.keys())
    for idee in id2:
        if idee not in links: ids_to_nodes.pop(idee).attrib.pop("id", 0)
    # Remove <defs> without IDs
    head_defs = tr.find(SVG_ + "defs")
    defs_without_ids = [child for child in head_defs if "id" not in child.attrib]
    for child in defs_without_ids: head_defs.remove(child)
    if not len(head_defs): tr.getroot().remove(head_defs)

def rarify_orthellipses(tr):
    """Orthogonalises circles and ellipses with no stroke."""
    nodes_in_links = getreferences(tr)
    ellipses = list(tr.iter(SVG_ + "ellipse"))
    circles = list(tr.iter(SVG_ + "circle"))
    for ell in ellipses + circles:
        if ell not in nodes_in_links and "stroke:" not in ell.attrib.get("style", ""):
            transtr = ell.get("transform", "")
            if ell in circles and transtr or ell in ellipses and not rotate1_re.fullmatch(transtr):
                attrs = {attr: ell.attrib.pop(attr) for attr in ("cx", "cy", "r", "rx", "ry", "transform") if attr in ell.attrib}
                newtag, newattrs = ellipt.orthogonalise(ell.tag, attrs)
                ell.tag = SVG_ + newtag
                ell.attrib.update(newattrs)

def rarify_reducetams(tr):
    """Minimises length of transforms in the node tree."""
    for node in tr.findall(".//*[@transform]"):
        reduced = mt.crush(node.attrib.pop("transform"))
        if reduced: node.set("transform", reduced)

def rarify_roundsws(tr, flags):
    """Rounds stroke widths, possibly slightly off due to floating-point error arising from grouping and ungrouping transformed objects, to a fixed number of decimal places."""
    def subf(m):
        round_float = round(float(m.group(1)), flags.swprec)
        if round_float.is_integer():
            round_float = int(round_float)
        return f"stroke-width:{round_float}"
    for node in tr.findall(".//*[@style]"): node.set("style", sw_re.sub(subf, node.get("style")))

def rarify_presentation(tr):
    """Splits style into presentation attributes if the latter is shorter (<= 3 properties)."""
    for node in tr.findall(".//*[@style]"):
        styprops = popstyle(node, True)
        if 0 < len(styprops) <= 3: node.attrib.update(styprops)
        if len(styprops) > 3: setstyle(node, styprops)

def rarify(tr, flags):
    """Rarifies the given SVG tree, applying individual reductions in order."""
    rarify_prunetree(tr, flags)
    rarify_node(tr, flags)
    rarify_references(tr)
    rarify_orthellipses(tr)
    rarify_reducetams(tr)
    rarify_roundsws(tr, flags)
    rarify_presentation(tr)
