def bezier_fromsvg(head, data, cursor, rpoint):
    """Constructs the corresponding Bézier curve from the given arguments.
    head is a character in LHVCSQT and their lowercases; data is a list of floats.
    cursor is self-explanatory; rpoint is the absolute reflected control point used by the S/T commands.
    Returns the Bézier object and the new cursor/rpoint."""
    if head in "Hh": pts = [complex(data[0] + (cursor.real if head == "h" else 0), cursor.imag)]
    elif head in "Vv": pts = [complex(cursor.real, data[0] + (cursor.imag if head == "v" else 0))]
    else:
        pts = [complex(*z) for z in zip(*[iter(data)] * 2)]
        if head.islower(): pts = [pt + cursor for pt in pts]
        if head in "SsTt": pts = [rpoint] + pts
    pts = [cursor] + pts
    return bezier(*pts), pts[-1], 2 * pts[-1] - pts[-2] if head in "CcSsQqTt" else pts[-1]

def ellipt_fromsvg(head, data, cursor):
    """Constructs the corresponding elliptical arc from the given SVG path data. Returns the arc and the new cursor."""
    end = complex(data[5], data[6]) + (cursor if head == "a" else 0)
    if cursor == end: return None
    if data[0] == 0 or data[1] == 0: return bezier(cursor, end)
    rx, ry, theta, large, sweep = abs(data[0]), abs(data[1]), data[2], bool(data[3]), bool(data[4])
    endprime = mt.scale(1 / rx, 1 / ry) @ mt.rotate(-theta) @ (end - cursor)
    middle = endprime / 2
    midabs, midarg = polar(middle)
    if middleabs >= 1:
        res = ellipt(mt.transcale(midabs, midabs, middle.real, middle.imag), [midarg + (-1) ** sweep * np.pi, midarg])
    else:
        centre = rect(1, midarg + (-1) ** (large == sweep) * np.arccos(midabs))
        t = [np.angle(-centre), np.angle(endprime - centre)]
        if t[0] > t[1] == sweep: t[min(t) > 0 != sweep] += (-1) ** (min(t) > 0) * 2 * np.pi
        res = ellipt(mt.translate(centre.real, centre.imag), t)
    res = mt.rotate(theta) @ mt.scale(rx, ry) @ res
    return res, end

# The path format is a list of closing indices followed by the segments themselves (numbered from one).
# Positive integers mean ends of an open path, negative ones that of a closed path.
# e.g. [[-4, 7, -8, 9], s1, ..., s9] <=> 1-2-3-4-close 5-6-7 8-close 9
# The lineto implied by a z on a path with non-coincident endpoints is made explicit here.
class path:
    strides = {'M': 2, 'Z': 0, 'L': 2, 'H': 1, 'V': 1, 'C': 6, 'S': 4, 'Q': 4, 'T': 2, 'A': 7}
    def __init__(self, d):
        self.segs, cursor, rpoint = [[0]], 0, 0
        for command in pcomm_re.finditer(d):
            head, data = command.groups()
            datas = zip(*[float(n) for n in num_re.findall(data)] * path.strides[head.upper()])
            if head in "Mm":
                cursor = complex(*next(datas)) + (cursor if head == 'm' else 0)
                if len(self.segs) - 1 != abs(self.segs[-1]): self.segs[0].append(len(self.segs) - 1)
                ll = chr(ord(head) - 1)
                for data in datas:
                    mint = bezier(ll, data, cursor, rpoint)
                    self.segs.append(mint[0])
                    cursor, rpoint = mint[1], mint[2]
            elif head in "Zz":
                start, end = self.segs[abs(self.segs[0][-1]) + 1](0), self.segs[-1](1)
                if abs(start - end) > 1e-9 * abs(start): self.segs.append(bezier(end, start))
                self.segs[0].append(1 - len(self.segs))
                cursor = rpoint = start
            elif head in "Aa":
                for data in datas:
                    mint = ellipt_fromsvg(head, data, cursor)
                    if mint:
                        self.segs.append(mint[0])
                        cursor = rpoint = mint[1]
            else:
                for data in datas:
                    mint = bezier_fromsvg(head, data, cursor, rpoint)
                    self.segs.append(mint[0])
                    cursor, rpoint = mint[1], mint[2]
        self.segs = self.segs[1:]

def parsepath(p):
    out = ""
    for headload in pcomm_re.finditer(p):
        head, load = headload.groups()
        load = flt(*[float(n) for n in num_re.findall(load)])
        out += head + load
    print(out)
