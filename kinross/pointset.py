# Operations on point sets
import numpy as np
import random
rng = random.SystemRandom()
from cmath import rect
from collections import deque

def chull(points):
    """Compute the convex hull of the given distinct points
    using the Graham scan. Return indices of hull points running
    counterclockwise from the leftmost-then-lowest point,
    including collinear ones."""
    if len(points) < 3:
        raise ValueError("at least three points needed")
    start = np.argmin(points)
    out = [start]
    
    def kfunc(n):
        if n == start:
            return -np.inf
        v = points[n] - points[start]
        return v.imag / abs(v)
    queue = sorted(range(len(points)), key=kfunc)
    out.append(queue[1])
    for n in queue[2:]:
        pn = points[n]
        while len(out) > 1:
            p1, p2 = points[out[-2]], points[out[-1]]
            v, w = p2 - p1, pn - p2
            if v.real * w.imag <= v.imag * w.real:
                out.pop()
            else:
                break
        out.append(n)
    return out

def bridson(w, h):
    """Generates random points in [0, w) × [0, h) separated by at least 1 using Bridson's algorithm."""
    # http://www.cs.ubc.ca/~rbridson/docs/bridson-siggraph07-poissondisk.pdf
    mesh = 0.70710678118654757
    offsets = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, 1), (-1, -1), (1, -1), (2, 0),
    (0, 2), (-2, 0), (0, -2), (2, 1), (1, 2), (-1, 2), (-2, 1), (-2, -1), (-1, -2), (1, -2), (2, -1)]
    grid = np.full((int(h / mesh) + 5, int(w / mesh) + 5), -1-1j)
    start_y, start_x = h * rng.random(), w * rng.random(); start = complex(start_x, start_y)
    grid[int(start_y / mesh) + 2,int(start_x / mesh) + 2] = start; active = [start]; yield start
    while active:
        pivot, dead = rng.choice(active), True
        for q in range(32):
            tooclose = False
            cand = pivot + rect(np.sqrt(rng.uniform(1, 4)), 2 * np.pi * rng.random())
            if not (0 <= cand.real < w and 0 <= cand.imag < h): continue
            cand_y, cand_x = int(cand.imag / mesh) + 2, int(cand.real / mesh) + 2
            if grid[cand_y,cand_x] != -1-1j: continue
            for offset_x, offset_y in offsets:
                contrapoint = grid[cand_y + offset_y,cand_x + offset_x]
                if contrapoint != -1-1j and abs(cand - contrapoint) < 1: tooclose = True; break
            if tooclose: continue
            grid[cand_y,cand_x] = cand; active.append(cand); yield cand; dead = False; break
        if dead: active.remove(pivot)
    return

def wolfe1(alpha, f0, f0_, fa, c = 1e-4):
    """Tests for the first Wolfe or Armijo condition: ϕ(α) ≤ ϕ(0) + cαϕ'(0)."""
    return fa <= f0 + c * alpha * f0_
def wolfe2(f0_, fa_, c = 0.9):
    """Tests for the strong second Wolfe or curvature condition: |ϕ'(α)| ≤ c|ϕ'(α)|."""
    return abs(fa_) <= c * abs(f0_)

def geommedian_distinct(points, w):
    """Geometric median of input distinct points (represented as a 2D ndarray), which may be of arbitrary dimension.
    w is an ndarray of corresponding positive weights. Uses Newton's method with Fletcher's line search
    (Algorithms 3.5 and 3.6 in Nocedal & Wright's Numerical Optimization, 2nd edition)."""
    N, d = points.shape
    # Check if any input point is itself the median. This happens to handle collinear cases too, where the median may not be unique.
    for i in range(N):
        vecs = points - points[i]
        vecls = np.linalg.norm(vecs, axis=1)
        vecls[i] = 1
        r = np.linalg.norm(w / vecls @ vecs)
        if r <= w[i]: return points[i]
    def fjh(x): # Returns the function value, Jacobian and Hessian at once
        vecs = x - points
        vecls = np.linalg.norm(vecs, axis=1)
        f = w @ vecls
        k = vecs.T / vecls
        j = (k * w).sum(axis=1)
        l = np.apply_along_axis(lambda v: np.eye(d) - np.outer(v, v), 0, k)
        h = (l * w / vecls).sum(axis=2)
        return (f, j, h)
    x = w @ points / w.sum()
    for q in range(32):
        fx, jx, hx = fjh(x)
        delta = np.linalg.solve(hx, -jx)
        def phi(alpha): # ϕ(α) for line search; caches data to avoid recomputation
            xa = x + alpha * delta
            fa, ja, ha = fjh(xa)
            return (alpha, fa, ja @ delta, (xa, fa, ja, ha))
        aq = deque([(0, fx, jx @ delta, (x, fx, jx, hx))], 2)
        f0_ = aq[0][2] # ϕ'(0)
        alpha, accepted, zoom_lo, zoom_hi = 1, None, None, None
        for r in range(3): # Main line search (3.5, p. 60)
            aq.append(phi(alpha))
            if not wolfe1(alpha, fx, f0_, aq[1][1]) or r and aq[1][1] >= aq[0][1]: zoom_lo, zoom_hi = aq[0], aq[1]; break
            if wolfe2(f0_, aq[1][2]): accepted = aq[1]; break
            if aq[1][2] >= 0: zoom_lo, zoom_hi = aq[1], aq[0]; break
            alpha *= 2
        if not accepted and zoom_lo: # Zoom procedure (3.6, p. 61)
            if abs(f0_) <= 1e-12: return x # Guard against plateaus that form as a result of finite-precision arithmetic
            for r in range(10):
                alpha = (zoom_lo[0] + zoom_hi[0]) / 2
                zoom_mi = phi(alpha)
                if not wolfe1(alpha, fx, f0_, zoom_mi[1]) or zoom_mi[1] >= zoom_lo[1]: zoom_hi = zoom_mi
                else:
                    if wolfe2(f0_, zoom_mi[2]): accepted = zoom_mi; break
                    if zoom_mi[2] * (zoom_hi[0] - zoom_lo[0]) >= 0: zoom_hi = zoom_lo
                    zoom_lo = zoom_mi
        if accepted:
            x, fx, jx, hx = accepted[3]
            if np.max(np.abs(jx)) <= 1e-12: break
        else: break
    return x

def geommedian(points, weights = None):
    """Geometric median of input points. Ensures simplicity of the point set passed to geommedian_distinct
    by combining coincident points into a single point of higher weight; also ignores non-positive-weighted points."""
    p, w = [], []
    for i in range(len(points)):
        cp, cw = points[i], 1 if weights is None else weights[i]
        if cw <= 0: continue
        if not p: p.append(cp); w.append(cw)
        else:
            dists = [np.linalg.norm(q - cp) for q in p]
            c = np.argmin(dists)
            if dists[c] <= 1e-10 * np.linalg.norm(cp): w[c] += cw
            else: p.append(cp); w.append(cw)
    return geommedian_distinct(np.array(p), np.array(w))
