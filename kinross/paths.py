# Bézier curves, elliptical arcs, lines, ellipses and paths
import numpy as np
from functools import lru_cache
from cmath import rect, polar
from .matrices import mt
from .algebra import ccquad, fproot
from .regexes import pcomm_re, number_re, sf, ssf
from .bernpol import berneval, bernmul, bernraise, bernder
from .bernpol import bernroots, bernvdm, x_conic

class ellipt:
    def __init__(self, m, ends=None):
        """Initialise an ellipt instance from the mt taking the unit circle
        to this instance and an optional parameter interval [a, b] on the
        unit circle (default interval is [0, 2π])."""
        self.m = +m
        self.ends = [0, 2 * np.pi] if ends == None else list(ends)
    def __str__(self):
        """Return a string representation of this curve."""
        return f"ellipt({self.m}, {self.ends})"
    def __repr__(self):
        return str(self)
    
    # The following functions act similarly to their mt counterparts
    @property
    def lin(self):
        return self.m.lin
    @property
    def ux(self):
        return self.m.ux
    @property
    def uy(self):
        return self.m.uy
    @property
    def aff(self):
        return self.m.aff
    
    def __call__(self, t):
        """Evaluate the curve at parameters t, where t is translated to a parameter
        on the unit circle using self.ends."""
        t = np.array(t)
        sv = np.full_like(t, self.ends[0], dtype=float)
        ev = np.full_like(t, self.ends[1], dtype=float)
        rt = sv * (1 - t) + ev * t
        return self.m @ (np.cos(rt) + 1j * np.sin(rt))
    def __getitem__(self, z):
        """z is a slice. Return the curve reparametrised such that
        t = 0, 1 in the new curve are t = a, b in the old curve,
        where a, b default to 0, 1."""
        t0 = 0 if z.start == None else z.start
        t1 = 1 if z.stop == None else z.stop
        ns = self.ends[0] * (1 - t0) + self.ends[1] * t0
        ne = self.ends[0] * (1 - t1) + self.ends[1] * t1
        return ellipt(self.m, [ns, ne])
    def __neg__(self):
        """Reverse this curve and return it."""
        return ellipt(self.m, self.ends[::-1])
    def __rmatmul__(self, m):
        """Transform this curve according to the given mt instance."""
        return ellipt(m @ self.m, self.ends)
    
    @property
    @lru_cache()
    def orth(self):
        """Return an orthogonalised form of this curve, whose corresponding
        affine map from the unit circle has orthogonal columns in its linear
        part and positive determinant (orientation)."""
        u, sigma, vt = np.linalg.svd(self.m.lin)
        # Ensure u is a rotation
        if np.cross(*u) < 0:
            u[:,1] *= -1
            vt[1] *= -1
        m2 = +self.m
        m2.m[:2,:2] = u * sigma
        if self.ends == [0, 2 * np.pi]:
            return ellipt(m2)
        theta = np.arctan2(*vt[::-1,0])
        orien = np.copysign(1, np.cross(*vt))
        return ellipt(m2, [theta + orien * e for e in self.ends])
    
    def fit_circle(pts):
        """Fit a circle to the given (>= 3) points. Algorithm from
        Coope (1993), Circle fitting by linear and nonlinear least squares,
        Journal of Optimization Theory and Applications, 76 (2), 381."""
        if len(pts) < 3:
            raise ValueError("three points define a circle")
        pts = np.array(pts)
        A = np.stack([pts.real, pts.imag, np.ones_like(pts, dtype=float)], axis=1)
        b = pts.real ** 2 + pts.imag ** 2
        x, y, c = np.linalg.lstsq(A, b, rcond=None)[0]
        r = np.sqrt(c + (x * x + y * y) / 4)
        return ellipt(mt.transcale(r, r, x / 2, y / 2))
    def fit(pts):
        """Fit an ellipse to the given (>= 5) points. Algorithm from
        Halíř and Fluser (1998), Numerically Stable Direct Least Squares
        Fitting of Ellipses, Winter School of Computer Graphics, vol. 6
        (http://wscg.zcu.cz/wscg1998/papers98/Halir_98.ps.gz)."""
        if len(pts) < 5:
            raise ValueError("five points define an ellipse")
        pts = np.array(pts)
        d1 = np.stack([pts.real ** 2, pts.real * pts.imag, pts.imag ** 2], axis=1)
        d2 = np.stack([pts.real, pts.imag, np.ones_like(pts, dtype=float)], axis=1)
        s1, s2, s3 = d1.T @ d1, d1.T @ d2, d2.T @ d2
        T = -np.linalg.inv(s3) @ s2.T
        M = [0.5, -1, 0.5] * (s1 + s2 @ T)
        evs = np.linalg.eig(M[::-1])[1]
        a = evs[:,4*evs[0]*evs[2]>evs[1]**2].flatten()
        q = np.concatenate([a, T @ a])
        
        qm = np.array([[q[0], q[1] / 2, q[3] / 2],
                       [q[1] / 2, q[2], q[4] / 2],
                       [q[3] / 2, q[4] / 2, q[5]]])
        mqf = qm[:2,:2]
        detqm, detmqf = np.linalg.det(qm), np.linalg.det(mqf)
        eigenvalues, eigenvectors = np.linalg.eig(mqf)
        dirvecs = eigenvectors / np.sqrt(-detmqf / detqm * eigenvalues)
        centre = (np.linalg.inv(mqf) @ -qm[:2,2:])[:,0]
        return ellipt(mt(*dirvecs.flatten("F"), *centre))
    
    @property
    @lru_cache()
    def length_integrand(self):
        """Integrand for the arc length function."""
        return lambda t: np.hypot(*(self.lin @ [-np.sin(t), np.cos(t)]))
    def length(self, t2=1, t1=0):
        """Measure this curve's length over [t1, t2]."""
        st = self.ends[0] * (1 - t1) + self.ends[1] * t1
        et = self.ends[0] * (1 - t2) + self.ends[1] * t2
        sgn = np.copysign(1, self.ends[1] - self.ends[0])
        return ccquad(self.length_integrand, st, et) * sgn
    def t_length(self, target):
        """Compute t with self.length(t) = target."""
        bound = np.copysign(1, target)
        while abs(self.length(bound)) < abs(target):
            bound *= 2
        return fproot(lambda t: self.length(t) - target, 0, bound)
    def project(self, p):
        """Project p onto this curve and return the projection's t-value.
        
        If the underlying ellipse's coefficient array (self.m.c) is
        [a c e]
        [b d f]
        the trigonometric parametrisation of this ellipse is
        (x, y) = (a cos(t) + c sin(t) + e, b cos(t) + d sin(t) + f).
        (x, y) is the foot of a perpendicular from p = (px, py), thus
        a candidate for p's projection onto the ellipse, iff
        (x', y') · (x - px, y - py) = 0. Now consider the following
        matrix multiplication:
                               [a cos(t)   c sin(t)    e - px] } x - px (m1)
                               [b cos(t)   d sin(t)    f - py] } y - py
        [ c cos(t)    d cos(t)]
        [-a sin(t)   -b sin(t)]           m3 = m2 @ m1
             ^           ^
             x'          y' (m2)
        The resulting matrix's entries are monomials in sin(t) and cos(t);
        their sum is the aforementioned dot product. Rewriting cos(t) = u
        and sin(t) = v, it is clear we have a conic section (generally
        a hyperbola in this case).
        However, u and v obviously satisfy u²+v²=1, so finding candidate t
        reduces to finding the intersection of two conics."""
        m1 = (+self.m).c
        m1[:,2] -= [p.real, p.imag]
        m2 = np.rot90(self.lin * [-1, 1])
        m3 = m2 @ m1
        bb, dd, ee = (m3[0,1] + m3[1,0]) / 2, m3[0,2] / 2, m3[1,2] / 2
        oc = np.array([[m3[0,0], bb, dd],
                       [bb, m3[1,1], ee],
                       [dd, ee, 0]])
        feet = x_conic(oc, np.diag([1, 1, -1]))
        cand_t = self.inv(self.m @ feet)
        cands = list(cand_t[cand_t <= 1]) + [0, 1]
        return min(cands, key=lambda t: abs(self(t) - p))
    
    def inv(self, p):
        """p is a point or sequence of points ideally lying on this
        curve or the underlying ellipse. Return the smallest non-negative t
        such that self(t) == p elementwise, analogous to bezier.t_inv."""
        t = np.angle(~self.m @ p)
        l = self.ends[1] - self.ends[0]
        md = np.copysign(2 * np.pi, l)
        return (t - self.ends[0]) % md / l
    @property
    @lru_cache()
    def qmat(self):
        """Return the symmetric 3×3 matrix associated with this curve's
        underlying ellipse. The implicit form is closely related to this matrix."""
        xv, yv = (~self.m).c
        res = np.outer(xv, xv) + np.outer(yv, yv)
        res[2,2] -= 1
        return res
    
    def x_bez(self, other):
        """Compute the intersections of this curve with the Bézier curve other
        by calling other's x_ell()."""
        return other.x_ell(self)[:,::-1]
    def x_ell(self, other):
        """Compute the intersections between this curve and elliptical arc other
        using the pencil-of-conics method."""
        points = x_conic(self.qmat, other.qmat)
        e1_t, e2_t = self.inv(points), other.inv(points)
        vind = np.nonzero((0 <= e1_t) & (e1_t <= 1) & (0 <= e2_t) & (e2_t <= 1))[0]
        e1_t, e2_t = e1_t[vind], e2_t[vind]
        return np.stack([e1_t, e2_t], axis=1)
    
    # TODO spin off orthogonalise() into a separate module
    def orthogonalise(tag, attrs):
        """Ignoring style, orthogonalises the given circle/ellipse
        (represented by its tag and attribute dictionary)
        so only a simple rotation is used.
        Returns the new tag and attribute dictionary."""
        if tag.endswith("circle"): rs = mt.scale(float(attrs["r"]))
        else: rs = mt.scale(float(attrs["rx"]), float(attrs["ry"]))
        outmat = ellipt(mt.parse(attrs.get("transform", "")) @
                        mt.translate(float(attrs.get("cx", "0")), float(attrs.get("cy", "0"))) @ rs).orth.m
        rx, theta, ry, centre = *polar(complex(*outmat.ux)), abs(complex(*outmat.uy)), complex(*outmat.aff)
        rx, ry = sf(rx), sf(ry)
        if rx == ry: tagout, attrout = "circle", {"r": rx}
        else:
            tagout, attrout = "ellipse", {"rx": rx, "ry": ry}
            centre = centre * rect(1, -theta)
            theta = sf(np.degrees(theta))
            if theta != "0": attrout["transform"] = f"rotate({theta})"
        cx, cy = sf(centre.real), sf(centre.imag)
        if cx != "0": attrout["cx"] = cx
        if cy != "0": attrout["cy"] = cy
        return tagout, attrout

class bezier:
    powbasis = (np.array([[1, 0], [-1, 1]]),
                np.array([[1, 0, 0], [-2, 2, 0], [1, -2, 1]]),
                np.array([[1, 0, 0, 0], [-3, 3, 0, 0], [3, -6, 3, 0], [-1, 3, -3, 1]]))
    bernbasis = (np.array([[1, 0], [1, 1]]),
                 np.array([[1, 0, 0], [1, 1/2, 0], [1, 1, 1]]),
                 np.array([[1, 0, 0, 0], [1, 1/3, 0, 0], [1, 2/3, 1/3, 0], [1, 1, 1, 1]]))
    
    def __init__(self, *cpoints):
        """Initialise a bezier instance from the given control points,
        a sequence of 2 to 4 complex numbers. The control points are accessed
        as self.v."""
        if not (2 <= len(cpoints) <= 4):
            raise ValueError("bezier constructor requires two to four control points")
        self.v = np.array(cpoints)
        self.deg = len(cpoints) - 1
        # We also compute derivatives of v for faster retrieval
        self.dv = bernder(self.v)
        self.d2v = bernder(self.dv)
    def __str__(self):
        """Return a string representation of this curve."""
        return f"bezier({','.join([str(z).strip('()') for z in self.v])})"
    def __repr__(self):
        return str(self)
    
    def __call__(self, t):
        """Evaluate this curve at real parameter(s) t."""
        return berneval(self.v, t)
    def __getitem__(self, k):
        """If called with a number, return the control point with that index.
        If called with a slice [a:b], return the curve reparametrised such that
        t = 0, 1 in the new curve are t = a, b in the old curve,
        where a, b default to 0, 1."""
        if type(k) == slice:
            a = 0 if k.start == None else k.start
            b = 1 if k.stop == None else k.stop
            # Define the parameter transformation matrix
            ptm = np.zeros((self.deg + 1, self.deg + 1))
            for i in range(self.deg + 1):
                ptm[:i + 1,i] = np.polynomial.polynomial.polypow([a, b - a], i)
            newv = bezier.bernbasis[self.deg - 1] @ ptm @ bezier.powbasis[self.deg - 1] @ self.v
            return bezier(*newv)
        return self.v[k]
    def __neg__(self):
        """Reverse this curve and return it."""
        return bezier(*self.v[::-1])
    def __rmatmul__(self, m):
        """Transform this curve using the given mt instance."""
        return bezier(*(m @ self.v))
    
    @property
    @lru_cache()
    def infl(self):
        """Compute the t-values of a cubic curve's inflections, where the curvature
        vanishes or x'y'' - x''y' = 0. The property and cache are defined since
        they do not depend on any input."""
        if self.deg != 3:
            return []
        # CQ is the pseudoinverse of the quadratic-to-cubic degree elevation matrix,
        # which yields the least-squares solution to the inverse problem.
        # infl_pol is calculated as a cubic, but is really a quadratic,
        # so we left-multiply by CQ.
        CQ = np.array([[0.95, 0.15, -0.15, 0.05],
                       [-0.25, 0.75, 0.75, -0.25],
                       [0.05, -0.15, 0.15, 0.95]])
        infl_pol = CQ @ (bernmul(self.dv.real, self.d2v.imag) -
                         bernmul(self.dv.imag, self.d2v.real))
        return bernroots(infl_pol)
    @property
    @lru_cache()
    def cx(self):
        """For cubic curves containing a loop, compute the parameters t, u where
        B(t) = B(u), i.e. the parameters of self-intersection.
        The method name is both a phonetic abbreviation ("c"elf "x")
        and an ASCII depiction of a loopy curve.
        
        As Bézier curve geometry is invariant under affine transformations,
        we can restrict ourselves to some canonical form of the curve.
        Then, consider the coordinate polynomials
        x(t) = at³+bt²+ct+p
        y(t) = dt³+et²+ft+q
        and parameters of self-intersection as λ and μ. By definition,
        x(λ) - x(μ) = a(λ³-μ³)+b(λ²-μ²)+c(λ-μ) = 0
        y(λ) - y(μ) = d(λ³-μ³)+e(λ²-μ²)+f(λ-μ) = 0
        Dividing by the trivial solution λ = μ and expanding we get
        a(λ²+λμ+μ²)+b(λ+μ)+c = 0
        d(λ²+λμ+μ²)+e(λ+μ)+f = 0
        In the canonical form chosen here
        (https://pomax.github.io/bezierinfo/#canonical) we have
        (x-3)(λ²+λμ+μ²)+3(λ+μ) = 0
        y(λ²+λμ+μ²)-3(λ+μ)+3 = 0
        whereby eliminating λ²+λμ+μ² gives (-3-3y/(x-3))(λ+μ) + 3 = 0
        or λ+μ = (x-3)/(x+y-3), followed by λμ = (λ+μ)²+3/(x+y-3).
        λ and μ can now be found by Viète's formulas."""
        if self.deg != 3:
            return []
        vx, vy, vz = self[2] - self[1], self[1] - self[0], self[3] - self[0]
        try:
            x, y = np.linalg.solve([[vx.real, vy.real],
                                    [vx.imag, vy.imag]], [vz.real, vz.imag])
        except np.linalg.LinAlgError:
            return []
        if x > 1 or \
           4 * y > (x + 1) * (3 - x) or \
           x > 0 and 2 * y + x < np.sqrt(3 * x * (4 - x)) or \
           3 * y < x * (3 - x):
            return []
        rs = (x - 3) / (x + y - 3)
        rp = rs * rs + 3 / (x + y - 3)
        x1 = (rs - np.sqrt(rs * rs - 4 * rp)) / 2
        return sorted([x1, rp / x1])
    
    @property
    @lru_cache()
    def length_integrand(self):
        """Integrand for the arc length function."""
        dx, dy = self.dv.real, self.dv.imag
        norm = bernmul(dx, dx) + bernmul(dy, dy)
        return lambda t: np.sqrt(berneval(norm, t))
    def length(self, t2=1, t1=0):
        """Measure this curve's length over [t1, t2]."""
        if self.deg == 1:
            return abs(self[1] - self[0]) * (t2 - t1)
        return ccquad(self.length_integrand, t1, t2)
    def t_length(self, target):
        """Compute t with self.length(t) = target. This works even for lengths
        beyond the curve proper and negative lengths."""
        # Struzik search starting from t = ±1 to find a bound
        bound = np.copysign(1, target)
        while abs(self.length(bound)) < abs(target):
            bound *= 2
        return fproot(lambda t: self.length(t) - target, 0, bound)
    def project(self, p):
        """Project p onto this curve and return the projection's t-value.
        Besides the endpoints, the relevant parameters satisfy (x-p)x' + (y-p)y' = 0."""
        foot = self.v - p # the constant 1 is a vector of ones in any Bernstein basis
        feeteq = bernmul(foot.real, self.dv.real) + bernmul(foot.imag, self.dv.imag)
        cand_t = bernroots(feeteq) + [0, 1]
        return cand_t[abs(self(cand_t) - p).argmin()]
    
    @property
    @lru_cache()
    def t_inv(self):
        """Derive an inversion formula for this curve,
        t = (a_1 x + b_1 y + c_1) / (a_2 x + b_2 y + c_2).
        Return np.array([[a_1, b_1, c_1], [a_2, b_2, c_2]]) (Sederberg p. 205)."""
        if self.deg == 3:
            C = np.stack([self.v.real, self.v.imag, np.ones(4)], axis=1)
            # Multiplication in the Bernstein basis by a fixed polynomial can be
            # expressed as a matrix multiplication on the coefficients.
            # U and T are the matrices corresponding to multiplication by
            # <1, 1> and <0, 1> respectively where the variable polynomial is cubic.
            U = np.array([[1, 0, 0, 0], [0.25, 0.75, 0, 0], [0, 0.5, 0.5, 0],
                          [0, 0, 0.75, 0.25], [0, 0, 0, 1]])
            T = np.diag([0.25, 0.5, 0.75, 1])
            A = np.zeros((5, 6))
            A[:,:3] = -(U @ C)
            A[1:,3:] = T @ C
            return np.linalg.qr(A.T, "complete")[0][:,-1].reshape(2, 3)
        if self.deg == 2:
            Q = np.stack([self.v.real, self.v.imag, np.ones(3)], axis=1)
            return np.array([np.linalg.solve(Q, [0, 0.5, 1]), [0, 0, 1]])
        # the curve is linear, use the projection formula
        delta = self[1] - self[0]
        res = np.array([delta.real, delta.imag,
                       -delta.real * self[0].real - delta.imag * self[0].imag])
        return np.array([res / abs(delta) ** 2, [0, 0, 1]])
    @property
    @lru_cache()
    def implicit(self):
        """Compute an implicit representation of this curve. Coefficients
        correspond to monomials in the same order as returned by bernvdm()."""
        A = bernvdm(self.v.real, self.v.imag, self.deg)
        return np.linalg.qr(A, "complete")[0][:,-1]
    
    def x_bez(self, other):
        """Compute the intersection between this curve and curve other using
        implicitisation and inversion. Return an np.array with two columns,
        each row corresponding to an intersection on [0,1]×[0,1] and listing first
        this curve's parameter, then the other curve's."""
        # If one of the curves has smaller degree, that curve is implicitised
        # (ic) and the other one's x(t) and y(t) are subbed into it (tc)
        swap = self.deg > other.deg # swapping from ic = self and tc = other?
        ic, tc = (other, self) if swap else (self, other)
        tc_pol = bernvdm(tc.v.real, tc.v.imag, ic.deg).T @ ic.implicit
        tc_t = np.array(bernroots(tc_pol))
        xy = tc(tc_t)
        nd = ic.t_inv @ [np.real(xy), np.imag(xy), np.ones_like(xy, dtype=float)]
        ic_t = nd[0] / nd[1]
        vind = np.nonzero((0 <= ic_t) & (ic_t <= 1))[0]
        tc_t, ic_t = tc_t[vind], ic_t[vind]
        bundle = (tc_t, ic_t) if swap else (ic_t, tc_t)
        return np.stack(bundle, axis=1)
    def x_ell(self, other):
        """Similar to x_bez(), but other is an elliptical arc. This is solved
        easily by deriving the implicit form of other and substituting."""
        Q = other.qmat
        ici = np.array([Q[2,2], 2*Q[0,2], 2*Q[1,2], Q[0,0], 2*Q[0,1], Q[1,1]])
        tc_pol = bernvdm(self.v.real, self.v.imag, 2).T @ ici
        tc_t = np.array(bernroots(tc_pol))
        ic_t = other.inv(self(tc_t))
        vind = np.nonzero((0 <= ic_t) & (ic_t <= 1))[0]
        tc_t, ic_t = tc_t[vind], ic_t[vind]
        return np.stack([tc_t, ic_t], axis=1)
