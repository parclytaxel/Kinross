#!/usr/bin/env python3.7
import numpy as np
from kinback.paths import bezier
# A plotter of Newton fractals resulting from the intersection of two Bézier curves.
# Colour is the projection of (t, u) onto a square of side length 0.8 centred in the r + g + b = 3/2 slice of the RGB cube,
# multiplied by a factor for the number of iterations required.

c1 = bezier(2+17j, 51-13j, -14+18j, 28+6j)
c2 = bezier(1+10j, 30+24j, 31-17j, 15+11j)
width = 1000
height = 1000

def plotnewton(b1, b2):
    s = 0.56568542494923801 # 0.8 / sqrt(2)
    b1x, b1y, b2x, b2y = b1.pols[0], b1.pols[1], b2.pols[0], b2.pols[1]
    d1x, d1y, d2x, d2y = b1x.deriv(), b1y.deriv(), b2x.deriv(), b2y.deriv()
    f = open("fractal.ppm", 'wb')
    f.write(f"P6 {width + 1} {height + 1} 255\n".encode())
    for u0 in range(height + 1):
        print(u0)
        for t0 in range(width + 1):
            t, u = t0 / width, u0 / height
            for q in range(24):
                try: dt, du = np.linalg.solve([[d1x(t), -d2x(u)], [d1y(t), -d2y(u)]], [b2x(u) - b1x(t), b2y(u) - b1y(t)])    
                except np.linalg.LinAlgError: qq, tt, uu = 0, 0, 0; break
                if np.hypot(dt, du) < 1e-12:
                    if 0 <= t <= 1 and 0 <= u <= 1: qq, tt, uu = 1 - q / 24, float(t - 0.5), float(u - 0.5) / 1.7320508075688772
                    else: qq, tt, uu = 1.5, 0, 0
                    break
                t, u = t + dt, u + du
                if q == 23: qq, tt, uu = 0, 0, 0; break
            red = round(255 * qq * (0.5 + (uu + tt) * s))
            green = round(255 * qq * (0.5 + (uu - tt) * s))
            blue = round(255 * qq * (0.5 - 2 * uu * s))
            f.write(bytes([red, green, blue]))
    f.close()
plotnewton(c1, c2)
