#!/usr/bin/env python3.7
from kinback.paths import ellipt
from kinback.matrices import tam
import numpy as np
N = 120

header = '''<svg xmlns="http://www.w3.org/2000/svg" viewBox="-300 -300 600 600">
  <ellipse rx="200" ry="100" fill="none" stroke="#000" stroke-width="2"/>\n'''
base = ellipt(tam.scale(200, 100))
for i in range(N):
    start = round((np.cos(2 * np.pi * i / N) + 1j * np.sin(2 * np.pi * i / N)) * 250, 8)
    end = round(base(base.projection(start)), 8)
    header += f'  <path d="M{start.real} {start.imag} {end.real} {end.imag}" fill="none" stroke="#000" stroke-width="2"/>\n'
header += "</svg>"
with open("normals.svg", 'w') as f: f.write(header)
