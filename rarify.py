#!/usr/bin/env python3
# Rarify, the uncouth SVG optimiser
import os, time, argparse, glob
import xml.etree.ElementTree as ET
from kinross.rarifyphases import rarify
from kinross.constants import SVG_NAMESPACES
for k, v in SVG_NAMESPACES.items():
    ET.register_namespace(k, v)

cdl = argparse.ArgumentParser(prog="./rarify.py", description="Rarify, the uncouth SVG optimiser")
cdl.add_argument("-m", "--metadata", action="store_false", default=True, help="keep metadata")
cdl.add_argument("-s", "--scripts", action="store_false", default=True, help="keep scripts")
cdl.add_argument("-d", "--dimens", action="store_true", default=False, help="remove width and height")
cdl.add_argument("-l", "--lpecrush", action="store_true", default=False, help="remove LPE output (this will break the picture outside Inkscape if it has LPEs)")
cdl.add_argument("-x", "--xml", action="store_true", default=False, help="add XML header")
cdl.add_argument("--swprec", default=2, type=int, help="round stroke widths to this many (default %(default)s) decimal places")
cdl.add_argument("files", nargs="*", help="list of files to rarify (if left blank, defaults to all SVG files in current directory)")
flags = cdl.parse_args()
flist = flags.files
if not flist:
    flist = glob.glob("*.svg")
for f in flist:
    if f.endswith("-rarified.svg"):
        continue
    tree = ET.parse(f)
    begin = time.perf_counter()
    rarify(tree, flags)
    end = time.perf_counter()
    outfn = f"{f[:-4]}-rarified.svg"
    with open(outfn, 'w') as outf:
        if flags.xml:
            outf.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n')
        ET.indent(tree)
        tree.write(outf, "unicode")
    before, after = os.path.getsize(f), os.path.getsize(outfn)
    print(f"{f}: {1000*(end-begin):.3f} ms, {before} → {after} bytes ({after/before:.2%})")
